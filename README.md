Pr�sentation "K�nstliche Intelligenz - Zwischen Faszination und Ern�chterung" f�r die Kieler Open Source und Linux Tage am 20.9.2019.

Lizenzen
========

Bilder und Texte, die nicht von mir stammen, sind auf den einzelnen Folien mit Quellenangabe und Lizenz versehen. Meine Bilder und Texte stehen unter <a  href="http://creativecommons.org/licenses/by-sa/3.0/legalcode">CC-BY-SA-3.0</a>.